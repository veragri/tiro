PYTHON=python
CONVERT=convert
VERSION="0.3.2"

ifeq ($(OS),Windows_NT)
	CONVERT=magick convert
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Darwin)
		PYTHON=python3
    endif
endif

sysinstall: ~/.local/share/applications/tiro.desktop ~/.icons/tiro.png
	$(PYTHON) -m pip install --user .
	$(INSTALL_ICONS)

~/.local/share/applications/tiro.desktop: src/tiro/data/tiro.desktop
	mkdir -p $(@D)
	cp -u $< $@
	update-desktop-database

~/.icons/tiro.png: src/tiro/data/tiro.png
	mkdir -p $(@D)
	cp -u $< $@
	update-desktop-database

.PHONY: docs
docs:
	cd docs && make html

build_exe:
	$(PYTHON) cxfreeze-setup.py build

build_msi:
	$(PYTHON) cxfreeze-setup.py bdist_msi

build_app:
	$(PYTHON) setup.py py2app
	cd dist && zip -r tiro-$(VERSION).zip tiro.app

install:
	$(PYTHON) -m pip install .

test:
	$(PYTHON) -m pytest

clean:
	rm -rf dist build *.spec
