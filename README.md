Please refer to the (rather short)
[documentation](https://patkun.ch/tiro/) (but then again Tiro is a
little program) and the [releases
page](https://gitlab.com/veragri/tiro/-/releases).
