from setuptools import find_namespace_packages

import sys
# from setuptools import setup
from cx_Freeze import setup, Executable

from tiro_setup import config

### MSI STUFF
base = None
if sys.platform == "win32":
    base = "Win32GUI"

# http://msdn.microsoft.com/en-us/library/windows/desktop/aa371847(v=vs.85).aspx
shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "Tiro",           # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]gui.exe",# Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     #"[TARGETDIR]src/tiro/data/tiro.ico",                     # Icon
     "", # ICON
     0,                     # IconIndex
     None,                     # ShowCmd
     'TARGETDIR'               # WkDir
     )
    ]

# Now create the table dictionary
msi_data = {"Shortcut": shortcut_table}

setup(
        name=config["name"],
    version=config["version"],
    app=["src/tiro/gui.py"],
    author=config["author"],
    author_email=config["author_email"],
    description=config["description"],
    entry_points={
        "gui_scripts": [
            "tiro = tiro.gui:main",
            ]
        },
    package_data=config["package_data"],
    package_dir={"": "src"},
    install_requires=[package[0] for package in config["packages"]],
    packages=find_namespace_packages(where="src"),

    # Win32-specific
    setup_requires=["cx_Freeze"],
    executables=[Executable("src/tiro/gui.py",
                            base=base,
                            # target_name="Tiro", # somehow this is broken
                            icon="src/tiro/data/tiro.ico",
                            )],
    options = {
        "build_exe": {
            "includes": ["tiro","tiro.crossword"],
            "packages": [package[1] for package in config["packages"]],
            "include_files":[(p,p) for p in config["data_dirs"]]},
            #"include_files":[("src/tiro/data","src/tiro/data")
            #                 ,("src/tiro/crossword/data","src/tiro/crossword/data")]},
        "bdist_msi": {
            "data": msi_data,
            "upgrade_code": "{c197d0a2-c998-4cf8-b901-6bab2fe4e379}",
            "install_icon": "src/tiro/data/tiro.ico"
        }
    }
)
