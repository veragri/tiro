.. Tiro documentation master file, created by
   sphinx-quickstart on Thu Dec 24 02:17:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tiro's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   usage
   Project <https://gitlab.com/veragri/tiro>
   Releases <https://gitlab.com/veragri/tiro/-/releases/>
   API Reference <source/modules>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
