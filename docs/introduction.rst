.. figure:: _static/prose-comp-data.png

Introduction
============

Tiro has been built to be a quick and unobtrusive tool for a teacher
to create exercises for students. The focus is on quick quizzing,
practice and drilling of propositional knowledge. As such, Tiro is
intended to aid the teacher in incorporating practice and recall
sequences into their lessons. It is designed with Latin teaching in
mind, but will in large part be suited for other disciplines as
well. Tiro is not a tool to create entire lesson plans or digital
learning units, nor is it a flashcard tool for students. The program
only deals with text, no image data. Tiro is a teacher‘s tiny Swiss
Army Knife for everyday use. It takes simple tabular data and turns it
into

- multiple-choice quizzes (for online quiz platforms),
- classroom drills (for display on a projector),
- self-checking exercises (as single-page HTML files), and
- crossword puzzles and straightforward tests (on paper).

The project was born out of tedium: Writing multiple-choice quizzes is
a chore as one question prompt requires one to come up with perhaps
three or more phony answers in addition to the correct one. It may be
relatively straightforward to add one false alternative (especially
when the difference between the correct and the wrong answer
illustrate a particular point), but putting together a full quiz with
sets of four possible answers per question quickly becomes a
time-consuming undertaking. Tiro is, amongst other things, a
multiple-choice quiz generator: The program takes a simple two-column
table and, with a bit of shuffling, creates a multiple-choice question
for each row. The program’s other functionality follow this same
principle: Turn simple tabular data into challenges for students.

Another form of tedium addressed by this program occurs at times when
a whole class has finished an exercise and answers are then checked
individually using an answer key or in a teacher-led plenary
discussion. Depending on the ’big thing’ the exercise was about, it
may well be that we intend to encourage a discourse about a particular
problem or topic and lifting the veil at the end of the learning
sequence is precisely what we want. But it is also possible that we
simply just drilled some knowledge in order to help automate recall,
using lesson time for practice: in this case ceremonious
answer-checking is likely at least a little tedious. Furthermore, if
we are simply practising unambiguous propositional knowledge, we might
just as well use a computer to check the answers. Tiro is a tool to
create focused exercises with unambiguous answers, in which students‘
responses are checked against the question-and-answer table via a
simple string match. Students go at their own pace and receive
immediate feedback.

Design Principle
----------------

The core principle of this software is quite straightforward. A
teacher keeps their own spreadsheets with tabular exercise data, with
a prompt in the A column and the corresponding answer in the B
column. The teacher builds their tables of questions and answers in
their spreadsheet software and arranges and saves the resulting files
as and where they like.

The Tiro window contains controls at the top and three tables that
mimic a spreadsheet application. The key table is titled ‘Question and
Answer Data’: Any output produced by Tiro is based on this data, the
idea being that the user copies data from their spreadsheet software
into the ‘Question and Answer Data’ area.

Having fed the data to Tiro, the user then selects one of the output
formats (Kahoot, Quizizz, Drill, Gapfill, Latin Prose Composition
Drill, Crossword or Paper Test), possibly adapts some options given
underneath the format bar and clicks the ‘generate’ button. Tiro’s
output should then open automatically in the default application for
the selected output.

Tiro’s output takes the forms of Word Processing documents (odt),
spreadsheet files (xlsx) or HTML files. Once Tiro has opened such an
output file in another application, it is up to the user to save a
copy of the file where they wish. Tiro is merely concerned with
generating quizzes and challenges from user-supplied data. The program
does not impose any organisation of input or output files. This is why
there is not even as much as a file dialog.
