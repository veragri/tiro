tiro package
============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tiro.crossword
   tiro.papertest
   tiro.prosecompdrill

Module contents
---------------

.. automodule:: tiro
   :members:
   :undoc-members:
   :show-inheritance:
