Usage
=====

Supplying Data
--------------

Supplying ‘Question and Answer Data’
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most commonly, the input data you supply will be a two-column table,
such as the sample data loaded when you open the program. By default,
the first column contains the question prompts, whereas answer columns
are to the right. The answer column contains the information for which
you would like to practise recall.

.. figure:: _static/two-column-data.png

   A two-column question-answer table supplied by the user.


It is also possible to enter multi-column data, especially when using
the *Drill* and *Gapfill* output formats. The functionality is
intended for exercises in memorising sequences of information, such as
paradigms or principal parts.

.. figure:: _static/multi-column-data.png

   Multi-column data.

What is the ‘Answer Pool’?
^^^^^^^^^^^^^^^^^^^^^^^^^^

The answer pool is a table of the same structure as the input data
(question and answer columns), but including false or intentionally
misleading responses.

What is the ‘Name List’?
^^^^^^^^^^^^^^^^^^^^^^^^

Add student names for use in some of the output formats.

What about the options?
^^^^^^^^^^^^^^^^^^^^^^^

Upon selecting an output format, the set of available options may
change. Some options change the input data, some the program output.

*reverse*
    This option reverses the input data, i.e. switches prompts and answers.
    
*shuffle*
    shuffles the input data rows.
    
*unidecode*
    currently replaces letters with breve and macron diacritics (e.g. ă or ō) with their bare counterparts (a or o).

Where are the resulting output files?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Where relevant, output files are saved in your Downloads
directory. The multiple-choice quiz spreadsheets can then be uploaded
to the corresponding online platforms. HTML output files can be shared
like any other file with students (possibly problematic with Android
or iOS tablets) or (ideally) uploaded to a simple webserver (your
school may provide webspace and help). The HTML files are
self-contained and work each on their own when accessed on a
webserver.

Generating a Multiple-Choice Quiz
---------------------------------

The *Kahoot* and *Quizizz* output formats generate and open an Excel
file ready for upload to the corresponding online platform. The
multiple-choice answer sets are built from the answer column(s) and
the answer pool and alternatives are included according to
similarity. The similarity measure may lead to some unexpected
results, which can easily be amended in the resulting spreadsheet.

*compare_prompts*
    This option changes the similarity score used to include or exclude answer alternatives. With this option selected, it is no longer the answers that are compared, but the question prompts.


Generating a Classroom Drill
----------------------------

The *Drill* format generates and opens an HTML file displaying one
question prompt at a time. This webpage can be shown on a projector
and used to orchestrate a quick classroom drill. Clicking the page or
pressing the spacebar moves through questions and answers. If there is
a sequence of answers in the input data, answers are displayed in
steps. If the name list contains names, these names are shown at
random at each step, which can help to activate all students in the
classroom. Of course, care should be taken to use this as a playful
exercise rather than a pillory. A drill like this works well as a
lesson starter or intermission.

Generating a Gapfill
--------------------

The *Gapfill* format recreates the input table as in an HTML file, but
leaves gaps for students to fill in. The HTML page checks students’
answers as they type. The text field background disappears once an
answer is correct. This checking method is of course very crude, but
does the job if prompts and answers are carefully chosen.

Generating a Latin Prose Composition Drill
------------------------------------------

.. figure:: _static/prose-comp-data.png

   A two-column question-answer table for Latin prose composition.


This uses two-column input data to create a self-checking Latin prose
composition exercise in an HTML page. The checking mechanism is
similarly crude as in the gapfill output format, but makes use of the
fact that word order in Latin is flexible. Students type their
translation into Latin and a percentage count indicates to them how
much of the result they have achieved so far. Of course, the checking
mechanism can be fooled in a variety of ways, which is why a 100%
score results in the solution being displayed. This has proven to be
good enough for the purpose of practising simple Latin syntax and
morphology. It is a good idea to break longer sentences up in its
clauses and provide guidance in the prompts.

Generating Crosswords and Paper Tests
-------------------------------------

Paper crosswords as well as simple, tabular paper tests are generated
using these output formats. The output files are in the OpenDocument
format, which can be opened in Microsoft Word, but have been tested
primarily in LibreOffice. Results in Microsoft Word may be a little
off.
