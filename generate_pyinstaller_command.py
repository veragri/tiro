#!/usr/bin/env python

import os

sep = os.pathsep

from setup_data import files,version

cmd = "pyinstaller -n \"tiro-{}\" -w --additional-hooks-dir=hooks --hidden-import=\"tiro\" --add-data \"src/tiro/data{}data\" --add-data \"src/tiro/crossword/data{}crossword/data\"".format(version,sep,sep)

#for f in files:
#    dest = f.replace("src/tiro/","")
#    cmd += " --add-data \"{}{}{}\"".format(f,sep,dest)

cmd += " src/tiro/gui.py"
print(cmd)
