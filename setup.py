from setuptools import find_namespace_packages

import sys, os, site
from setuptools import setup
from setuptools.command.install import install
from pkg_resources import resource_string,resource_filename

from tiro_setup import config

### POST INSTALL


def installation_directory():
    """Return the installation directory, or None"""
    if '--user' in sys.argv:
        paths = (site.getusersitepackages(),)
    else:
        py_version = '%s.%s' % (sys.version_info[0], sys.version_info[1])
        paths = (s % (py_version) for s in (
            sys.prefix + '/lib/python%s/dist-packages/',
            sys.prefix + '/lib/python%s/site-packages/',
            sys.prefix + '/local/lib/python%s/dist-packages/',
            sys.prefix + '/local/lib/python%s/site-packages/',
            '/Library/Python/%s/site-packages/',
        ))

    for path in paths:
        if os.path.exists(path):
            return path
    print('no installation path found', file=sys.stderr)
    return None

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        install.run(self)
        from pyshortcuts import make_shortcut
        make_shortcut(os.path.join(installation_directory(),"tiro/gui.py"),name="Tiro")

# APP STUFF

py2app_options = {
    'argv_emulation': False,
    'iconfile': 'src/tiro/data/tiro.icns',
    'plist': {
        'CFBundleDisplayName': config["name"],
        'CFBundleName': config["name"],
        # 'CFBundleGetInfoString': "",
        'CFBundleIdentifier': "ch.patkun.osx.tiro",
        'CFBundleVersion': config["version"],
        'CFBundleShortVersionString': config["version"],
        # 'NSHumanReadableCopyright': u"Copyright © 2015, Chris Hager, All Rights Reserved"
}}

# SETUP

setup(
    name=config["name"],
    version=config["version"],
    app=["src/tiro/gui.py"],
    author=config["author"],
    author_email=config["author_email"],
    description=config["description"],
    entry_points={
        "gui_scripts": [
            "tiro = tiro.gui:main",
            ]
        },
    package_data=config["package_data"],
    package_dir={"": "src"},
    install_requires=[package[0] for package in config["packages"]],
    packages=find_namespace_packages(where="src"),
    cmdclass={
        'install': PostInstallCommand,
    },
    setup_requires=['py2app'],
    data_files = config["data_files"],
    options={"py2app": py2app_options},
)


