version = "0.32"

tiro_package_data = ["data/linguamix.html",
                     "data/wandtafelfussball.html",
                     "data/sample_input.tsv",
                     "data/sample_input_multiple_columns.tsv",
                     "data/gapfill.html",
                     "data/about.txt",
                     "data/fitty.min.js",
                     "data/jquery-3.4.1.min.js",
                     "data/jquery.md5.js",
                     "data/quiz-icon.png",
                     "data/kahoot-custom-quiz-template.xlsx",
                     "data/quizizz-custom-quiz-template.xlsx"]

tiro_crossword_package_data = ["data/LiberationMono-Regular.ttf",
                               "data/LiberationSans-Regular.ttf"]

files = ["src/tiro/" + p for p in tiro_package_data] + ["src/tiro/crossword/" + p for p in tiro_crossword_package_data]

packages = [
    ("argparse","argparse"),
    ("unidecode","unidecode"),
    ("jellyfish","jellyfish"),
    ("Pillow","PIL"),
    ("odfpy","odf"),
    ("ttkthemes","ttkthemes"),
    ("openpyxl","openpyxl"),
    ("tksheet","tksheet"),
    ("prosecompdrill","prosecompdrill")
]
