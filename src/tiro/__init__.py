import tiro.crossword
import tiro.papertest

import argparse,sys,csv,io,re,os,json,webbrowser
from unidecode import unidecode
from random import choice,shuffle,randint,randrange
from jellyfish import jaro_winkler_similarity,levenshtein_distance
import tempfile,subprocess,shlex
import difflib
import random

from hashlib import md5
import os
from importlib import resources
from openpyxl import load_workbook
import copy

from tiro.prosecompdrill import make_prosecompdrill

from pathlib import Path


class Tiro():
    """The Tiro class. Instantiates an object containing question-answer
    data passed to the constructor. See
    :attr:`Tiro.add_to_question_data` for details.

    :param input_data: Two-dimensional list of strings.
    :type input_data: List, optional, defaults to empty list.

    """
    
    # __version__ = pkg_resources.get_distribution("tiro").version
    __version__ = "0.3.2"
    
    # Prompts and Answers
    __prompt_index = 0
    __answer_index = 1


    # Settings

    __format_settings = dict()
    __format_settings["all"] = ["reverse"
                                ,"shuffle"
                                ,"unidecode"
                                ,"compare_unidecode"
                                ,"first_word"
                                ,"compare_first_word"]
    __format_settings["kahoot"] = ["compare_prompts","compare_endings","compare_random"]
    __format_settings["quizizz"] = __format_settings["kahoot"]
    __format_settings["gapfill"] = ["random_ask"]
    __format_settings["prosecompdrill"] = []
    __format_settings["linguamix"] = []
    __format_settings["wandtafelfussball"] = []
    __format_settings["papertest"] = ["random_ask","answers"]
    __format_settings["crossword"] = []

    def get_format_settings(self,frmt):
        """Return the relevant settings for a given output format.

        :param frmt: A string representing an output format, i.e.
            `kahoot`, `quizizz`, `gapfill`, `prosecompdrill`,
            `linguamix`, `wandtafelfussb all`, `papertest`,
            `crossword`.
        :type frmt: str

        """
        try:
            return self.__format_settings[frmt]
        except:
            return []
    
#        elif '_MEIPASS2' in os.environ:
#        __data_dir = os.path.join(os.environ['_MEIPASS2'],"data")



    def __get_datafile_dir(module="tiro"):
        data_dir = ""
        if '_PY2APP_LAUNCHED_' in os.environ:
            data_dir = os.environ["RESOURCEPATH"]
        elif getattr(sys,"frozen",False):
            data_dir = os.path.join(os.path.dirname(sys.executable),
                                    "src",
                                    module.replace(".","/"),
                                    "data")
        else:
            with resources.path(module,"__init__.py") as p:
                abspath = os.path.abspath(str(p))
                data_dir = os.path.join(os.path.dirname(abspath),"data")
        return data_dir

    def __open_datafile(module,path):
        fullpath = Tiro.__get_datafile_path(module,path)
        return open(fullpath,'r',encoding='utf8').read()

    def __get_datafile_path(module,path):
        fullpath = os.path.join(Tiro.__get_datafile_dir(module),path)
        return fullpath

    def __get_first_word(instring,space_only=True):
        pattern = re.compile(' +')
        if not space_only:
            pattern = re.compile('\W+')
        return re.split(pattern,instring)[0].rstrip(", ")
    
    def __unidecode(instring):
        table = [
            ["ā","a"],
            ["ē","e"],
            ["ī","i"],
            ["ō","o"],
            ["ū","u"],
            ["Ā","A"],
            ["Ē","E"],
            ["Ī","I"],
            ["Ō","O"],
            ["Ū","U"],
            ["ă","a"],
            ["ĕ","e"],
            ["ĭ","i"],
            ["ŏ","o"],
            ["ŭ","u"],
            ["Ă","A"],
            ["Ĕ","E"],
            ["Ĭ","I"],
            ["Ŏ","O"],
            ["Ŭ","U"],
        ]

        for pair in table:
            instring = instring.replace(pair[0],pair[1])
        return instring
    
    # constructor
    def __init__(self,input_data=[]):
        self.prompt_index = 0
        self.answer_index = 1
        self.question_data = list()
        self.question_data_dict = dict()
        self.data_pool = list()
        self.extra_data = list()
        self.namelist = list()
        self.row_length = 0
        self.__settings = {"reverse": False,
                           "random_ask": False,
                           "shuffle": False,
                           "unidecode": False,
                           "compare_unidecode": True,
                           "first_word": False,
                           "compare_first_word": True,
                           "compare_prompts": False,
                           "compare_random": False,
                           "compare_endings": False,
                           "answers": False}
        self.add_to_question_data(input_data)



    def set_setting(self,setting,value):
        """
        Set an output setting to `True` or `False`.  Not all settings
        are relevant for all output formats.  The attribute
        :attr:`Tiro.__format_settings` shows which settings are relevant
        for which output format, if any.

        :param setting: A setting.  Valid setting strings are
            "reverse", "random_ask", "shuffle", "unidecode",
            "compare_prompt" and "answers".
        :type setting: str

        :param value: A boolean value for the setting.
        :type value: bool
        """

        
        if setting not in self.__settings.keys():
            raise Exception("{} is not a valid setting.".format(setting))
        if not type(value) is bool:
            raise TypeError("Setting values are always booleans.")
        self.__settings[setting] = value

    def get_setting(self,setting):

        """
        Get a setting value.

        :param setting: A setting.  Valid setting strings are
            "reverse", "random_ask", "shuffle", "unidecode",
            "compare_prompt" and "answers".
        :type setting: str

        :return: The boolean stored for the given setting.
        :rtype: bool
        """
        
        if setting not in [self.__settings.keys()]:
            raise Exception("{} is not a valid setting.".format(setting))
        return self.__settings[setting]

    def get_setting_keys(self):
        """
        Return the list of valid setting keys.

        :return: List of valid setting keys.
        :rtype: list
        """
        return self.__settings.keys()




        
    def clear_data(self):
        """
        Clears question data as well as data pool and name list.
        """
        
        self.question_data = list()
        self.question_data_dict = dict()
        self.data_pool = list()
        self.extra_data = list()
        self.namelist = list()
        self.row_length = 0
    
    def add_to_question_data(self,input_data):
        """
        Add two-dimensional list to question data.  The first column
        is the initial prompt, the second column provides the answer.
        Further columns extend the answer to a sequence.  This is
        relevant for the drill and gapfill output format.

        `input_data` is automatically added to the pool (see
        :attr:`Tiro.add_to_data_pool`), i.e. for multiple-choice
        quizzes, wrong answers are taken from other questions.

        :param input_data:
        :type input_data: two-dimensional list of strings
        """

        
        self.question_data += self.get_data(input_data)
        self.update_row_length()
        self.update_question_data_dict()
        self.add_to_data_pool(Tiro.split_choices(input_data))

    def remove_choices(input_data):
        output_data = list()
        for i in input_data:
            prompt = i[Tiro.__prompt_index]
            answer = i[Tiro.__answer_index]
            new_item = ["",""]
            new_item[Tiro.__prompt_index] = prompt
            new_item[Tiro.__answer_index] = answer.split("/")[0]
            output_data.append(new_item)
        return output_data

    def split_choices(input_data):
        output_data = list()
        for i in input_data:
            prompt = i[Tiro.__prompt_index]
            answer = i[Tiro.__answer_index]
            for a in answer.split("/"):
                new_item = ["",""]
                new_item[Tiro.__prompt_index] = prompt
                new_item[Tiro.__answer_index] = a
                output_data.append(new_item)
        return output_data
    
    def update_question_data_dict(self):
        if self.row_length > 2:
            input_data = Tiro.reduce_data(self.question_data)
        else:
            input_data = self.question_data
        for row in input_data:
            prompt = row[self.__prompt_index]
            answer = row[self.__answer_index].split("/")[0]
            if prompt in self.question_data_dict:
                if not answer in self.question_data_dict[prompt]:
                    self.question_data_dict[prompt].append(answer)
            else:
                self.question_data_dict[prompt] = [answer]

    def update_row_length(self):
        row_length = 0
        for row in self.question_data:
            if len(row) > row_length:
                row_length = len(row)
        self.row_length = row_length
        
    def get_data(self,input_data,onecolumn=False):
        data = list()
        for row in input_data:
            datarow = copy.deepcopy(row)
            if self.__settings["reverse"]:
                datarow = datarow[::-1]
            data.append(datarow)
        if len(data) < 1:
            return data
        for row in data:
            if row and len(row) < 2 and not onecolumn:
                row.append("_")
        if self.__settings["shuffle"]:
            shuffle(data)
        if self.__settings["unidecode"] or self.__settings["first_word"]:
            for i in range(len(data)):
                for j in range(len(data[i])):
                    if self.__settings["unidecode"]:
                        data[i][j] = Tiro.__unidecode(data[i][j])
                    if self.__settings["first_word"] and j > 0:
                        data[i][j] = Tiro.__get_first_word(data[i][j])
        return data

    def add_to_data_pool(self,pool_data):
        """
        Add two-dimensional list to data pool.  The data pool is used
        to populate multiple-choice quizzes with wrong, but similar
        answers.  Pool data should have the same shape as question
        data, i.e. columns correspond.

        :param pool_data:
        :type pool_data: two-dimensional list of strings
        """
        
        data_pool = self.get_data(pool_data)
        self.data_pool = self.data_pool + data_pool

    def add_to_namelist(self,name_data):
        """
        Add list of names to the name list.  The name list is used in
        drill and ‘Wandtafelfussball’ activities.

        :param name_data:
        :type name_data: list of strings
        """
        
        namelist = self.get_data(name_data,True)
        self.namelist = self.namelist + namelist

    def reduce_data(data):
        """
        Class method to reduce question data with multiple answer
        columns to a two-column list.  The prompt is the sequence with
        a random gap, the answer is the missing element.  An example
        illustrates this the best: ``["to
        touch","tangere","tango","tetigi","tactum"]`` becomes ``["to
        touch • … • tetigi • tactum","tango"]``.

        :param data: The two-dimenstional question-answer data, with
            sequences of answers in multiple columns.
        :type data: two-dimenstional list of strings

        :return: two-dimensional list of strings with merely
                 two-columns
        :rtype: list
        """
        
        input_data = copy.deepcopy(data)
        row_length = 0
        output_data = list()
        for row in input_data:
            if len(row) > row_length:
                row_length = len(row)

        if row_length <= 3:
            output_data = copy.deepcopy(input_data)
            return output_data

        for row in input_data:
            i = randrange(0,len(row))
            answer = row[i]
            row[i] = "…"
            question = " • ".join(row)
            output_data.append([question,answer])

        return output_data

    def reduce_pool(data):
        """
        Class method to reduce question data with multiple answer
        columns to a two-column list.  This method ensures that all
        the answers in all sequences are added to the pool of the
        primary answer column.

        :param data: The two-dimenstional question-answer data, with
            sequences of answers in multiple columns.
        :type data: two-dimenstional list of strings

        :return: two-dimensional list of strings with merely
                 two-columns
        :rtype: list
        """
        
        input_data = copy.deepcopy(data)
        row_length = 0
        output_data = list()
        for row in input_data:
            if len(row) > row_length:
                row_length = len(row)

        if row_length <= 2:
            output_data = copy.deepcopy(input_data)
            return output_data

        for row in input_data:
            first = row[0]
            for i in range(0,len(row)):
                new_row = [None,None]
                new_row[Tiro.__prompt_index] = first
                new_row[Tiro.__answer_index] = row[i]
                output_data.append(new_row)

        return output_data

    def generate_kahoot(self,quizizz=False):
        kahoot_data = self.generate_kahoot_data(quizizz)
        Tiro.generate_and_open_quiz_template(kahoot_data,quizizz)

    def generate_kahoot_data(self,quizizz=False):
        """
        Generates multiple-choice data based on the added question
        data and pool data (for wrong, but similar answers).  Each
        question is output with four possible answers.

        :param quizizz: A boolean determining whether to output five
            possible answers instead of the default four.
        :param type: bool

        :return: Multiple-choice data (two-dimensional list of
                 strings).
        :rtype: list
        """
        
        num_of_choices = 4
        if quizizz:
            num_of_choices = 5
        kahoot_data = list()
        pool_data = list()
        input_data = copy.deepcopy(self.question_data)
        row_length = 0
        for row in input_data:
            if len(row) > row_length:
                row_length = len(row)

        if row_length > 2:
            input_data = Tiro.reduce_data(self.question_data)
            self.add_to_data_pool(Tiro.reduce_pool(input_data))
            
        for item in input_data:
            data_pool = copy.deepcopy(self.data_pool)
            prompt = item[Tiro.__prompt_index]
            if prompt in [row[0] for row in kahoot_data]:
                continue
            answers = item[Tiro.__answer_index].split("/")
            answer = answers[0]
            kahoot_item = list()
            kahoot_item.append(prompt)
            if quizizz:
                kahoot_item.append("Multiple Choice")
            choices = list()
            choices.append(answer)

            compare_idx = Tiro.__answer_index
            if self.__settings["compare_prompts"]:
                compare_idx = Tiro.__prompt_index

            def prep_compare_string(instring):
                comp_string = instring
                if self.__settings["compare_unidecode"]:
                    comp_string = Tiro.__unidecode(comp_string)
                if self.__settings["compare_first_word"]:
                    comp_string = Tiro.__get_first_word(comp_string,False)
                return comp_string
                
            def compare_strings(x):
                a = x[compare_idx]
                b = item[compare_idx]
                comp_a = prep_compare_string(a)
                comp_b = prep_compare_string(b)
                if self.__settings["compare_random"]:
                    return random.random()
                elif self.__settings["compare_endings"]:
                    return jaro_winkler_similarity(comp_a,comp_b)
                else:
                    return difflib.SequenceMatcher(None,comp_a,comp_b).ratio()
            
            data_pool = sorted(data_pool,key=compare_strings)

            if len(answers) > 1:
                for other_answer in answers[1:]:
                    if len(choices) < num_of_choices:
                        choices.append(other_answer)
                    
            while len(choices) < num_of_choices:
                pick = data_pool.pop()
                pick_answer = pick[Tiro.__answer_index]
                pick_prompt = pick[Tiro.__prompt_index]
                comp_pick_answer = prep_compare_string(pick_answer)
                comp_pick_prompt = prep_compare_string(pick_prompt)
                comp_prompt = prep_compare_string(prompt)
                if (comp_pick_answer in [prep_compare_string(item) for item in choices]) or (comp_pick_answer == comp_prompt):
                    continue
                if quizizz and comp_pick_prompt == comp_prompt:
                    continue
                choices.append(pick_answer)

            shuffle(choices)
            correct_indices = []
            for i in range(0,len(choices)):
                if choices[i] in self.question_data_dict[prompt]:
                    correct_indices.append(i+1)
                i += 1
            kahoot_item = kahoot_item + choices
            correct_str = ",".join([str(i) for i in correct_indices])
            if not quizizz:
                kahoot_item = kahoot_item + ["10"] + [correct_str]
            else:
                kahoot_item = kahoot_item + [correct_str] + ["10"]
            kahoot_data.append(kahoot_item)

        return kahoot_data

    def generate_and_open_quiz_template(kahoot_data,quizizz=False):

        """
        Takes multiple-choice data generated by
        :attr:`Tiro.generate_kahoot_data` and opens a corresponding
        spreadsheet in the default desktop application.

        :param kahoot_data: Multiple-choice data generated by
            :attr:`Tiro.generate_kahoot_data`.
        :type kahoot_data: list

        :param quizizz: A boolean determining whether to output a
            spreadsheet based on five-answer questions rather than the
            default four.
        :param type: bool
        """

        
        xlsx_template_filename = "kahoot-custom-quiz-template.xlsx"
        row_offset = 9
        column_offset = 2
        prefix = "kahoot_"
        sheet = "Sheet1"

        if quizizz:
            xlsx_template_filename = "quizizz-custom-quiz-template.xlsx"
            row_offset = 3
            column_offset = 1
            prefix = "quizizz_"
            sheet = "Create a Quiz"
        
        xlsx_file = Tiro.__get_datafile_path("tiro",xlsx_template_filename)
        wb = load_workbook(filename = xlsx_file)
        # ws = wb.get_active_sheet()
        ws = wb.get_sheet_by_name(sheet)
        for index,row in enumerate(kahoot_data):
            for r in range(len(row)):
                ws.cell(row=index+row_offset
                        ,column=r+column_offset).value = row[r]

        

        tmpdir = None

        downloads_dir = os.path.join(str(Path.home()),"Downloads")
        if os.path.isdir(downloads_dir):
            tmpdir = downloads_dir
        
        tmpxlsxfile=tempfile.mkstemp(prefix=prefix
                                     ,suffix=".xlsx"
                                     ,dir=tmpdir)
        wb.save(tmpxlsxfile[1])
        Tiro.__os_open(tmpxlsxfile[1])

    def generate_odt_crossword(self):
        input_data = copy.deepcopy(Tiro.remove_choices(self.question_data))
        row_length = 0
        for row in input_data:
            if len(row) > row_length:
                row_length = len(row)

        if row_length > 2:
            input_data = Tiro.reduce_data(input_data)
        
        cw = crossword.Crossword(30,28,"-",2000,input_data)
        cw.compute_crossword()
        cw.get_crossword_odt()        


    def generate_gapfill(self,text=""):
        tmpdir = None

        downloads_dir = os.path.join(str(Path.home()),"Downloads")
        if os.path.isdir(downloads_dir):
            tmpdir = downloads_dir
        tmpfile=tempfile.mkstemp(prefix="gapfill_",suffix=".html",dir=tmpdir)
        html = Tiro.__open_datafile("tiro","gapfill.html")
        jquery_js = Tiro.__open_datafile("tiro","jquery-3.4.1.min.js")
        jquery_md5 = Tiro.__open_datafile("tiro","jquery.md5.js")
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---JQUERY",jquery_js)
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---MD5",jquery_md5)
        if self.__settings["random_ask"]:
            html = html.replace("randomAsk = false","randomAsk = true")
        if self.__settings["shuffle"]:
            html = html.replace("shuffleOption = false","shuffleOption = true")
        if len(text) > 0:
            html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---JSON","[]")
            html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---TEXT","\""+self.parse_gapfill(text)+"\"")
        else:
            html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---JSON",json.dumps(Tiro.remove_choices(self.question_data)))
            html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---TEXT","\"\"")
        
        with open(tmpfile[0],'w',encoding="utf8") as f:
            f.write(html)
        f.close()
        Tiro.__os_open(tmpfile[1])
        return tmpfile[1]

    def generate_linguamix(self):
        tmpfile=tempfile.mkstemp(suffix=".html")
        html = Tiro.__open_datafile("tiro","linguamix.html")
        jquery_js = Tiro.__open_datafile("tiro","jquery-3.4.1.min.js")
        fitty_js = Tiro.__open_datafile("tiro","fitty.min.js")
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---JQUERY",jquery_js)
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---FITTY",fitty_js)
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---JSON",json.dumps(Tiro.remove_choices(self.question_data)))
        html = html.replace("f51670ac-082b-4908-8dae-ea0054bb0218---NAMELIST-JSON",json.dumps(self.namelist))
        if self.__settings["shuffle"]:
            html = html.replace("shuffleOption = false","shuffleOption = true")
        with open(tmpfile[0],'w',encoding="utf8") as f:
            f.write(html)
        f.close()
        Tiro.__os_open(tmpfile[1])
        return tmpfile[1]

    def generate_wandtafelfussball(self):
        tmpfile=tempfile.mkstemp(suffix=".html")
        html = Tiro.__open_datafile("tiro","wandtafelfussball.html")

        html = html.replace("\"f51670ac-082b-4908-8dae-ea0054bb0218---NAMELIST-JSON\"",json.dumps(self.namelist))
        with open(tmpfile[0],'w',encoding="utf8") as f:
            f.write(html)
        f.close()
        Tiro.__os_open(tmpfile[1])
        return tmpfile[1]    

    def generate_papertest(self,copies=1):
        pt = papertest.PaperTest(Tiro.remove_choices(self.question_data))
        pt.get_papertest_document(copies,self.__settings["shuffle"],self.__settings["random_ask"],self.__settings["answers"])

    def __os_open(url):
        if sys.platform=='win32':
            os.startfile(url)
        elif sys.platform=='darwin':
            subprocess.Popen(['open', url])
        else:
            try:
                subprocess.Popen(['xdg-open', url])
            except OSError:
                print('Manually open the following file: '+url)

    def generate_prosecompdrill(self):
        tmpdir = None

        downloads_dir = os.path.join(str(Path.home()),"Downloads")
        if os.path.isdir(downloads_dir):
            tmpdir = downloads_dir
        tmpfile=tempfile.mkstemp(prefix="prosecomp_",suffix=".html",dir=tmpdir)
        html = make_prosecompdrill(Tiro.remove_choices(self.question_data))
        with open(tmpfile[0],'w',encoding="utf8") as f:
            f.write(html)
        f.close()
        Tiro.__os_open(tmpfile[1])
        return tmpfile[1]
