#/usr/bin/env python3

import sys

from tiro import Tiro

from tkinter import *
from tkinter.ttk import *
# from ttkthemes import ThemedTk
from tkinter import filedialog
from tksheet import Sheet

import csv
import copy
import io,os
import tempfile,webbrowser,json
import re


def get_tsv_data(path):
    tsv_data = list()
    path = Tiro._Tiro__get_datafile_path("tiro",path)
    with open(path,encoding="utf8") as tsvfile:
        tsvreader = csv.reader(tsvfile,delimiter="\t")
        tsv_data = list(tsvreader)
    return copy.deepcopy(tsv_data)

def remove_empty_data(data):
    input_data = [row for row in data if any(row)]
    output_data = list()
    for row in input_data:
        j = len(row)-1
        for i in range(len(row)-1,-1,-1):
            if row[i]:
                j=i+1
                break
        output_data.append(row[0:j])
    return output_data
            
            

class Application(Frame):

    # font
    # f = ("Liberation Mono",12)

    # controls
    controls_height = 1

    def __init__(self, master=None):

        # init
        super().__init__(master)
        self.master = master
        self.pack(expand=True,fill=BOTH)


        # styling
        style = Style(self)
        style.configure("TButton"
                        #,font=self.f
                        )
        style.configure("TCheckbutton"
                        #,font=self.f
                        )
        style.configure("TRadiobutton"
                        #,font=self.f
                        )
        style.configure("Text"
                        #,font=self.f
                        )

        # text input list
        text_inputs = list()

        # tksheets

        tksheets_bindings = ("single_select", #"single_select" or "toggle_select"
                             "drag_select",   #enables shift click selection as well
                             "column_drag_and_drop",
                             "row_drag_and_drop",
                             "column_select",
                             "row_select",
                             "column_width_resize",
                             "double_click_column_resize",
                             #"row_width_resize",
                             #"column_height_resize",
                             "arrowkeys",
                             "row_height_resize",
                             "double_click_row_resize",
                             "right_click_popup_menu",
                             "rc_select",
                             "rc_insert_column",
                             "rc_delete_column",
                             "rc_insert_row",
                             "rc_delete_row",
                             "hide_columns",
                             "copy",
                             "cut",
                             "paste",
                             "delete",
                             "undo",
                             "edit_cell"
                             )

        rows=1000
        columns=100

        # main input
        sample_input = get_tsv_data("sample_input.tsv")
        input_frame = Frame(self)
        self.input_sheet = Sheet(input_frame
                                 ,total_rows = rows
                                 ,total_columns = columns
                                 ,width=None
                                 ,height=None
                                 )
        
        

        self.input_sheet.sheet_data_dimensions(total_rows=rows,total_columns=columns)
        for i in range(len(sample_input)):
            self.input_sheet.set_row_data(i,sample_input[i])
        self.input_sheet.enable_bindings(tksheets_bindings)

        Label(input_frame
              ,text="Question and Answer Data"
              ,anchor=W
              ,justify=LEFT).pack(side=TOP,expand=False,fill=X)
        self.input_sheet.pack(side=BOTTOM,expand=True,fill=BOTH)

        # pool input
        pool_frame = Frame(self)
        self.pool_sheet = Sheet(pool_frame
                                ,total_rows = rows
                                ,total_columns = columns)
        self.pool_sheet.sheet_data_dimensions(total_rows=rows,total_columns=columns)
        self.pool_sheet.enable_bindings(tksheets_bindings)
        Label(pool_frame
              ,text="Answer Pool"
              ,anchor=W
              ,justify=LEFT).pack(side=TOP,expand=False,fill=X)
        self.pool_sheet.pack(side=BOTTOM,expand=True,fill=BOTH)

        # format selection and generate button
        self.quiz_format = StringVar()
        self.quiz_format.set("linguamix")
        format_frame = Frame(self)
        format_buttons = list()

        formats = [
            ("kahoot","Kahoot")
            ,("quizizz","Quizizz")
            ,("linguamix","Drill")
            ,("wandtafelfussball","Wandtafelfussball")
            ,("gapfill","Gapfill")
            ,("prosecompdrill","Latin Prose Composition Drill")
            ,("crossword","Crossword")
            ,("papertest","Paper Test")
        ]

        for value, label in formats:
            button = Radiobutton(
                format_frame
                ,variable=self.quiz_format
                ,value=value
                ,text=label
                ,command=self.update_settings)
            format_buttons.append(button)

        generate_button = Button(format_frame
                                 ,command=self.generate_quiz
                                 ,text="generate")
        format_buttons.append(generate_button)

        for i in range(len(format_buttons)):
            format_buttons[i].grid(row=0,column=i)


        # copies input
        # self.copies_entry = Entry(self
                                  #,font=self.f
#                                  )
        # self.copies_entry.grid(row=1,column=4)

        # output
        self.status = StringVar()
        statusbar = Label(self
                          ,textvariable=self.status
                          ,relief=SUNKEN
                          ,anchor=W)


        # name list
        name_frame = Frame(self)
        self.name_sheet = Sheet(name_frame
                                ,total_rows = rows
                                ,total_columns = 1)
        self.name_sheet.sheet_data_dimensions(total_rows=rows,total_columns=1)
        self.name_sheet.enable_bindings(tksheets_bindings)
        Label(name_frame
              ,text="Name List"
              ,anchor=W
              ,justify=LEFT).pack(side=TOP,expand=False,fill=X)
        self.name_sheet.pack(side=BOTTOM,expand=True,fill=BOTH)
        
        # initialise quiz
        self.tiro = Tiro();

        # controls and settings
        settings_frame = Frame(self)
        self.settings_checkbuttons = dict()
        self.settings = dict()
        j = 0
        settings_per_row=4
        for key in self.tiro.get_setting_keys():
            self.settings[key] = IntVar()
            checkbutton = Checkbutton(settings_frame,
                                      text=key,
                                      offvalue=False,
                                      onvalue=True,
                                      variable=self.settings[key],
                                      command=self.update_settings,
                                      )
            self.settings_checkbuttons[key] = checkbutton
            r = j//settings_per_row
            c = j%settings_per_row
            checkbutton.grid(row=r,column=c,sticky=W)
            j=j+1

        # copies input -- what to do with this?
        # self.copies_entry = Entry(settings_frame)
        # r = j//settings_per_row
        # c = j%settings_per_row
        # self.copies_entry.grid(row=r,column=c,sticky=W)


        # Composition
        
        self.grid_columnconfigure(0,weight=4)
#        self.grid_columnconfigure(1,weight=1)
#        self.grid_rowconfigure(0,weight=1)
#        self.grid_rowconfigure(1,weight=1)
        self.grid_rowconfigure(2,weight=1)
        self.grid_rowconfigure(3,weight=1)
#        self.grid_rowconfigure(4,weight=1)


        format_frame.grid(row=0,column=0,columnspan=2,sticky=W+E)
        settings_frame.grid(row=1,column=0,columnspan=2,sticky=W+E)
        input_frame.grid(row=2,column=0,sticky=N+S+W+E)
        pool_frame.grid(row=3,column=0,sticky=N+S+W+E)
        name_frame.grid(row=2,column=1,rowspan=2,sticky=N+S+W+E)
        statusbar.grid(row=4,column=0,columnspan=2,sticky=W+E+S+N)

        # Menu
        menubar = Menu(self)
        filemenu = Menu(menubar,tearoff=0)
        # filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.quit)
        menubar.add_cascade(label="File", menu=filemenu)
        helpmenu = Menu(menubar,tearoff=0)
        # helpmenu.add_separator()
        helpmenu.add_command(label="Documentation", command=self.popup_docs)
        helpmenu.add_command(label="About", command=self.popup_showabout)
        menubar.add_cascade(label="Help", menu=helpmenu)        
        self.master.config(menu=menubar)

    def popup_docs(self):
        Tiro._Tiro__os_open("https://patkun.ch/tiro/usage.html")

    def popup_showabout(self):
        global message_window
        message_window = Toplevel()
        message_window.geometry("500x500")
        message_window.grid_propagate(False)
        message_window.grid_rowconfigure(0, weight=1)
        message_window.grid_columnconfigure(0, weight=1)
        message_window.title("About")
        text = Text(message_window,height=10, width=80)
        text.insert("1.0",Tiro._Tiro__open_datafile("tiro","about.txt"))
        text.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)
        scrollb = Scrollbar(message_window, command=text.yview)
        scrollb.grid(row=0, column=1, sticky='nsew')
        text['yscrollcommand'] = scrollb.set
        ok = Button(message_window,text="OK",command=message_window.destroy)
        ok.grid(row=1,column=0)

    def update_settings(self):
        for key in self.settings.keys():
            self.tiro.set_setting(key,False)
            if self.settings[key].get() == 1:
                self.tiro.set_setting(key,True)
        for key in self.settings_checkbuttons.keys():
            checkbutton = self.settings_checkbuttons[key]
            if key in self.tiro.get_format_settings(self.quiz_format.get()) or key in self.tiro.get_format_settings("all"):
                checkbutton.config(state=NORMAL)
            else:
                checkbutton.config(state=DISABLED)

    def generate_quiz(self):
        input_data = self.input_sheet.get_sheet_data()
        input_data = remove_empty_data(input_data)
        pool_data = self.pool_sheet.get_sheet_data()
        pool_data = remove_empty_data(pool_data)
        name_data = self.name_sheet.get_sheet_data()
        name_data = remove_empty_data(name_data)
        self.tiro.clear_data()
        self.tiro.add_to_question_data(input_data)
        self.tiro.add_to_data_pool(pool_data)
        self.tiro.add_to_namelist(name_data)
        #try:
        #    copies = int(self.copies_entry.get())
        #except ValueError:
        #    copies = 1
        copies = 1

        self.update_settings()

        output = str()
        if self.quiz_format.get() == "kahoot":
            output = self.tiro.generate_kahoot()

        elif self.quiz_format.get() == "quizizz":
            output = self.tiro.generate_kahoot(True)

        elif self.quiz_format.get() == "linguamix":
            output=self.tiro.generate_linguamix()

        elif self.quiz_format.get() == "wandtafelfussball":
            output=self.tiro.generate_wandtafelfussball()            

        elif self.quiz_format.get() == "crossword":
            output=self.tiro.generate_odt_crossword()

        elif self.quiz_format.get() == "papertest":
            output=self.tiro.generate_papertest(copies)

        elif self.quiz_format.get() == "gapfill":
            output=self.tiro.generate_gapfill()

        elif self.quiz_format.get() == "prosecompdrill":
            output=self.tiro.generate_prosecompdrill()

        self.status.set(output)
        return self.tiro



class TextInput(Text):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.bind('<Control-a>',TextInput.select_all)
        self.bind('<Control-A>',TextInput.select_all)
        self.bind('<<Paste>>',TextInput.custom_paste)
        #self.config(font=Application.f)

    @staticmethod
    def select_all(event=None):
        event.widget.tag_add('sel', '1.0', 'end')
        return "break"

    @staticmethod
    def custom_paste(event=None):
        try:
            event.widget.delete("sel.first", "sel.last")
        except:
            pass
        event.widget.insert("insert", event.widget.clipboard_get())
        return "break"

def main():
    name = "Tiro {}".format(Tiro.__version__)
#    window = ThemedTk(theme="arc",className=name)
    window = Tk(className=name)
    window.title(name)
    icon = PhotoImage(master=window, file=Tiro._Tiro__get_datafile_path("tiro","tiro.png"))
    window.wm_iconphoto(True, icon)
    app = Application(window)
    app.update_settings()
    app.mainloop()

if __name__=="__main__":
    main()
