import os,tempfile,sys
from random import shuffle,randint
from odf.opendocument import OpenDocumentText
from odf.style import Style, TextProperties, ParagraphProperties, FontFace
from odf.style import TableProperties,TableColumnProperties, TableRowProperties, TableCellProperties, PageLayout, PageLayoutProperties,MasterPage
from odf.text import H,P,Span,SoftPageBreak
from odf.table import Table, TableColumn, TableRow, TableCell


from pathlib import Path

class PaperTest():

    def __init__(self,data):
        self.data = data[:]


    def get_papertest_document(self,copies=1,shuf=False,random_ask=False,answers=False):
        data = self.data[:]
        textdoc = OpenDocumentText()
        textdoc.text.setAttribute("usesoftpagebreaks","true")
        tablecontents = Style(name="Table Contents", family="paragraph")
        tablecontents.addElement(ParagraphProperties(numberlines="false", linenumber="0"))
        tablecontents.addElement(TextProperties(fontfamily="Liberation Sans,Calibri,Arial,sans-serif"))
        textdoc.styles.addElement(tablecontents)

        pagelayout = PageLayout(name="PaperTestPageStyle")
        pagelayout.addElement(PageLayoutProperties(margin="1cm"))
        textdoc.automaticstyles.addElement(pagelayout)
        masterpage = MasterPage(name="Standard", pagelayoutname=pagelayout)
        textdoc.masterstyles.addElement(masterpage)
        tablestyle = Style(name="Table Style", family="table",masterpagename=masterpage)
        tablestyle.addElement(TableProperties(bordermodel="collapsing"))
        textdoc.automaticstyles.addElement(tablestyle)
        rowstyle = Style(name="Row Style", family="table-row")
        rowstyle.addElement(TableRowProperties(minrowheight="0.75cm"))
        textdoc.automaticstyles.addElement(rowstyle)
        cellstyle = Style(name="Text Cell", family="table-cell")
        cellstyle.addElement(TableCellProperties(border="0.5pt solid #000000",verticalalign="middle"))
        textdoc.automaticstyles.addElement(cellstyle)

        emptycellstyle = Style(name="Text Cell", family="table-cell")
        emptycellstyle.addElement(TableCellProperties(border="0.5pt solid #000000",verticalalign="middle",backgroundcolor="#444444"))
        textdoc.automaticstyles.addElement(emptycellstyle)

        for i in range(copies):
            if shuf:
                shuffle(data)
            table = Table(stylename=tablestyle)
            table.addElement(TableColumn(numbercolumnsrepeated=len(data[0])))
            for row in data:
                tr = TableRow(stylename=rowstyle)
                table.addElement(tr)
                randints = []
                if random_ask:
                    r = randint(0,len(row)-1)
                    while row[r] == "":
                        r = randint(0,len(row)-1)
                    for i in range(len(row)):
                        if i != r:
                            randints.append(i)
                elif answers:
                    randints = []
                else:
                    randints = list(range(1,len(row)))
                i = 0
                for field in row:
                    stylepick = cellstyle
                    if field == "":
                        stylepick = emptycellstyle
                    tc = TableCell(stylename=stylepick)
                    tr.addElement(tc)
                    text = field
                    if i in randints:
                        text = ""
                    p = P(stylename=tablecontents,text=text)
                    tc.addElement(p)
                    i += 1
            textdoc.text.addElement(table)
            textdoc.text.addElement(SoftPageBreak())

        tmpdir = None

        downloads_dir = os.path.join(str(Path.home()),"Downloads")
        if os.path.isdir(downloads_dir):
            tmpdir = downloads_dir
        tmpodtfile=tempfile.mkstemp(prefix="papertest_",suffix=".odt",dir=tmpdir)
        textdoc.save(tmpodtfile[1])
        if sys.platform.startswith('linux'):
            os.system("xdg-open {}".format(tmpodtfile[1]))
        elif sys.platform.startswith('darwin'):
            os.system("open {}".format(tmpodtfile[1]))
        elif sys.platform.startswith('win32'):
            os.startfile(tmpodtfile[1])
        return tmpodtfile[1]
