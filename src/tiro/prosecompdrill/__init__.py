from pathlib import Path
import base64
from unidecode import unidecode
import os
from importlib import resources
import sys
import tiro

writing_symbol = "✎"
unidecode_symbols = ["ā","ē","ī","ō","ū","ă","ĕ","ĭ","ŏ","ŭ",
                     "Ā","Ē","Ī","Ō","Ū","Ă","Ĕ","Ĭ","Ŏ","Ŭ"]


def limited_unidecode_char(character):
    if character in unidecode_symbols:
        return unidecode(character)
    else:
        return character

def limited_unidecode(string):
    unidecoded_string = str()
    for letter in string:
        unidecoded_string += limited_unidecode_char(letter)
    return unidecoded_string

def make_prosecompdrill(data):
    questionlist = ""
    for row in data:
        questionlist += "<li><p>{}</p>{}</li>".format(row[0],make_textarea(row[1]))
        
    html = tiro.Tiro._Tiro__open_datafile("tiro.prosecompdrill","template.html")
    html = html.replace("QUESTIONLIST",questionlist)
    return html

def make_textarea(orig_content):
    nlines = len(orig_content.split("&&")[0].split())//5
    if nlines < 1:
        nlines = 1
    simplified_content = limited_unidecode(orig_content)
    simplified_content_base64 = base64.b64encode(simplified_content.encode("utf-8")).decode("utf-8")
    return '<textarea class="prosecomp" data-solution="{}" placeholder="{}" value="" autocorrect="off" autocapitalize="none" cols=78 rows={}></textarea>'.format(simplified_content_base64,writing_symbol,nlines)
        
