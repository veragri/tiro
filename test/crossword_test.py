from tiro.crossword import Crossword
import csv,os


def test_crossword():
    data = [
        ["adicere","hinzufügen"],
        ["adiungere","hinzufügen"],
        ["coniungere","verbinden"],
        ["inicere","hineinwerfen"],
        ["castra","Lager"],
        ["hiberna","Winterlager"],
        ["adicere","hinzuwerfen"]
    ]
    cw = Crossword(30,28,"-",2000,data)
    cw.compute_crossword()
    cw.get_crossword_odt()

def test_crossword_error():
    data = [
        ["ich höre","audio"],
        ["du hörst","audis"],
        ["sie hört","audit"],
        ["wir hören","audimus"],
        ["ihr hört","auditis"],
        ["sie hören","audiunt"],
        ["hör!","audi"],
        ["hört!","audite"],
        ["ich komme","venio"],
        ["du kommst","venis"],
        ["er kommt","venit"],
        ["wir kommen","venimus"],
        ["ihr kommt","venitis"],
        ["sie kommen","veniunt"],
        ["komm!","veni"],
        ["kommt!","venite"],
        ["ich handle","ago"],
        ["du handelst","agis"],
        ["sie handelt","agit"],
        ["wir handeln","agimus"],
        ["ihr handelt","agitis"],
        ["sie handeln","agunt"],
        ["handle!","age"],
        ["handelt!","agite"],
    ]
    cw = Crossword(30,28,"-",2000,data)
    cw.compute_crossword()
    cw.get_crossword_odt()
        
