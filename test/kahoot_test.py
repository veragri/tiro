from tiro import Tiro

data = [
    ["adicere","hinzufügen"],
    ["adiungere","hinzufügen"],
    ["coniungere","verbinden"],
    ["inicere","hineinwerfen"],
    ["castra","Lager"],
    ["hiberna","Winterlager"],
    ["adicere","hinzuwerfen"]
]

def test_identical():
    tiro = Tiro()
    tiro.settings["reverse"] = True
    tiro.add_to_question_data(data)
    kahoot_data = tiro.generate_kahoot_data()
    for row in kahoot_data:
        if row[0] == "hinzufügen":
            correct_indices = []
            for i in range(1,len(row)):
                if row[i] == "adicere" or row[i] == "adiungere":
                    correct_indices.append(i)
            for i in correct_indices:
                assert i in [int(x) for x in row[6].split(",")]
    count = 0
    for row in kahoot_data:
        if row[0] == "hinzufügen":
            count += 1
    assert count == 1

def test_excel_template():
    tiro = Tiro()
    tiro.settings["reverse"] = True
    tiro.add_to_question_data(data)
    tiro.generate_kahoot()
    tiro.generate_kahoot(quizizz=True)
