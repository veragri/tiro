import os
config = {}

### VERSION AND INCLUDES

config["author"] = "Patrick Kuntschnik"
config["author_email"] = "patrick.kuntschnik@gmail.com"
config["description"] = "A quizzing tool for use in the classroom."
config["version"] = "0.3.2"
config["name"] = "tiro"

config["tiro_package_data"] = ["data/linguamix.html",
                     "data/wandtafelfussball.html",
                     "data/sample_input.tsv",
                     "data/sample_input_multiple_columns.tsv",
                     "data/gapfill.html",
                     "data/about.txt",
                     "data/fitty.min.js",
                     "data/jquery-3.4.1.min.js",
                     "data/jquery.md5.js",
                     "data/tiro.png",
                     "data/tiro.ico",
                     "data/tiro.icns",
                     "data/kahoot-custom-quiz-template.xlsx",
                     "data/quizizz-custom-quiz-template.xlsx"]

config["tiro_crossword_package_data"] = ["data/LiberationMono-Regular.ttf",
                               "data/LiberationSans-Regular.ttf"]

config["tiro_prosecompdrill_package_data"] = ["data/template.html"]

config["data_files"] = ["src/tiro/" + p for p in config["tiro_package_data"]] + ["src/tiro/crossword/" + p for p in config["tiro_crossword_package_data"]] + ["src/tiro/prosecompdrill/" + p for p in config["tiro_prosecompdrill_package_data"]]

config["data_dirs"] = list(set([os.path.dirname(p) for p in config["data_files"]]))

config["packages"] = [
    ("argparse","argparse"),
    ("unidecode","unidecode"),
    ("jellyfish","jellyfish"),
    ("Pillow","PIL"),
    ("odfpy","odf"),
    ("openpyxl","openpyxl"),
    ("tksheet","tksheet"),
    ("pyshortcuts","pyshortcuts")
]

config["package_data"] = {
    "tiro":config["tiro_package_data"],
    "tiro.crossword": config["tiro_crossword_package_data"],
    "tiro.prosecompdrill": config["tiro_prosecompdrill_package_data"],
}
